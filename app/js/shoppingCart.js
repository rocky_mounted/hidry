class Cart {
  constructor(productsList) {
    this._productsList = productsList;
    this._btnArr = document.querySelectorAll(".product-item__button");
    this._arrProductsId = [];
    this._btnArr.forEach((el) =>
      el.addEventListener("click", this.getId.bind(this))
    );
    this.getSavedProductsList();   //!!!!!
    this.itemProduct = document.querySelectorAll(".product-item");
    this.itemProduct.forEach((el) => this.countFunc(el));
    this.selectProducts()
    this.buttonDeleteAll = document
      .querySelector(".delete-all-item")
      .addEventListener("click", this.deleteAll.bind(this));
  }

  getId(e) {
    e.preventDefault();

    // отбираю нажатые карточки
    let productId = e.currentTarget.parentNode.parentNode.getAttribute(
      "data-id"
    );
    this._arrProductsId.push(Number(productId));

    let selectedCards = this._productsList.filter((el) => {
      return this._arrProductsId.includes(el.id);
    });
    // сохраняем отобранные карточки в localStorage
    localStorage.setItem("productItem", JSON.stringify(selectedCards));

    // рендерю попапы и меняю кнопки
    this.renderPopup(e);
    this.getSavedProductsList();
  }

  getSavedProductsList() {
    let newProductItem = JSON.parse(localStorage.getItem("productItem"));

    if (newProductItem && newProductItem.length !== 0) {
      this.showProductsCount(newProductItem);
      this.showProductsPrice(newProductItem);
      this.createSelectedElements(newProductItem);
    }
  }

  // показать колличество псов
  showProductsCount(list) {
    let count = document.querySelector(".header-cart__counter");
    count.innerText = ` (${list.length})`;
  }

  // показать стоимость псов
  showProductsPrice(list) {

    let totalPrice = document.querySelector(".header-cart__price");
    let numberTotalArr = this.getTotaPrice(list);
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    let numberTotal = numberTotalArr.reduce(reducer, 0);
    totalPrice.innerText = ` (${numberTotal}) ₽`;
  }

  renderPopup(el) {
    el.currentTarget.classList.remove("btn-orange");
    el.currentTarget.classList.add("btn-white");
    el.currentTarget.textContent = "В корзине";

    let popupAdd = document.createElement("div");
    popupAdd.classList.add("add-element");
    el.currentTarget.insertAdjacentElement("afterend", popupAdd);
    let popupShowId = setTimeout(function () {
      popupAdd.remove();
    }, 1000);
  }

  getTotaPrice(arr) {
    let priceList = [];
    arr.forEach((el) => {
      priceList.push(el.price);
    });
    return priceList;
  }

  createSelectedElements(arr) {
    let list = document.querySelector(".shopping-list");
    list.innerHTML = "";
    let productFragment = document.createDocumentFragment();
    let templateProductCard = document.getElementById("shopping-list__item");
    let sum = [];
    arr.forEach((el) => {
      const itemElement = templateProductCard.content.cloneNode(true);
      // itemElement.querySelector(".shopping-list__button").dataset.id = el.id;
      itemElement.querySelector("img").src = `../img/${el.photo}`;
      itemElement.querySelector(".shopping-list__title").textContent = el.name;
      itemElement.querySelector(
        ".shopping-list__item__price"
      ).textContent = `${el.price} ₽`;
      itemElement.querySelector(
        ".shopping-list__checkbox"
      ).id = `product_${el.id}`;
      itemElement
        .querySelector("label")
        .setAttribute("for", `product_${el.id}`);
      productFragment.appendChild(itemElement);

      let countProducts = document.querySelector(".countProducts");
      countProducts.textContent = arr.length;
      let priceAllProducts = document.querySelector(".priceAllProducts");

      sum.push(el.price);
      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      priceAllProducts.textContent = `${sum.reduce(reducer, 0)} ₽`;
    });

    list.appendChild(productFragment);
  }

  countFunc(el) {
    let btnPlus = el.querySelector(".btn-add");
    let btnMinus = el.querySelector(".btn-remove");
    let field = el.querySelector(".counter");
    let price = el.querySelector(".shopping-list__item__price");
    let buttonDeleteItem = el.querySelector(".shopping-list__button");

    let priceValue = price.textContent.slice(0, -1);
    let priceNumberValue = parseFloat(priceValue, 10);
    let fieldValue = parseFloat(field.value, 10);

    btnMinus.addEventListener("click", function () {
      if (fieldValue > 1) {
        fieldValue--;
        field.value = fieldValue;
        let newPrice = parseFloat(
          el
            .querySelector(".shopping-list__item__price")
            .textContent.slice(0, -1),
          10
        );
        let priceNewNumberValue = newPrice - priceNumberValue;
        console.log(newPrice);
        price.innerText = `${priceNewNumberValue} P`;
      } else {
        return 1;
      }
    });

    btnPlus.addEventListener("click", function () {
      fieldValue++;
      field.value = fieldValue;
      let priceNewNumberValue = priceNumberValue * field.value;
      // console.log(priceNumberValue)
      price.innerText = `${priceNewNumberValue} ₽`;
    });

    buttonDeleteItem.addEventListener("click", this.deleteItem.bind(this));

  }

  deleteItem(e) {
    let newProductItem = JSON.parse(localStorage.getItem("productItem"));
    let productsList = document.querySelectorAll(".product-item");

    for (let i = 0; i < productsList.length; i++) {
      let btn = productsList[i].querySelector(".shopping-list__button");
      btn.dataset.id = i;
    }
    let id = parseFloat(e.currentTarget.dataset.id, 10);

    if (id > -1) {
      newProductItem.splice(id, 1);
    }
    localStorage.clear();
    localStorage.setItem("productItem", JSON.stringify(newProductItem));
    this.createSelectedElements(newProductItem);

  }

  deleteAll() {
    this._arrProductsId = [];
    localStorage.clear();

    let newProductItem = JSON.parse(localStorage.getItem("productItem"));
    localStorage.setItem("productItem", JSON.stringify(newProductItem));

    let priceAllProducts = document.querySelector(".priceAllProducts");
    priceAllProducts.textContent = `0 ₽`;
    this.createSelectedElements(newProductItem);
  };

  selectProducts() {
    let buttonCheck = document.querySelectorAll(".check-item");
    let checkedProducts = []
    buttonCheck.forEach(el => el.addEventListener('change', function (e) {
      if (el.checked) {
        checkedProducts.push(el);
        let countProducts = document.querySelector(".countProducts");
        countProducts.textContent = checkedProducts.length;
      } if (!el.checked) {
        console.log('jdjd')
      }
    }))
  }

}




export function setCount(productsList) {
  new Cart(productsList)
}




// открытие/закрытие (наброски)
let shoppingCart = document.querySelector('.shopping-cart')
shoppingCart.addEventListener("click", shoppingCartVisible);

function shoppingCartVisible(e) {
  e.preventDefault()
  let a = document.querySelector(".shopping-cart__blackout");
  a.style.display = 'block'

}

let closeBtn = document.querySelector(".button-close");

closeBtn.addEventListener("click", closeShoppingCart);

function closeShoppingCart(e) {
  e.preventDefault();
  let a = document.querySelector(".shopping-cart__blackout");
  a.style.removeProperty('display');
}

