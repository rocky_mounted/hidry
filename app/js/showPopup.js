export function showPopupMessage(e) {
  const popupMessage = document.querySelector(".found-elements");
  let el = e.target;
  let location = el.getBoundingClientRect();

  popupMessage.classList.add("found-elements--visible");
  popupMessage.style.top = location.top + pageYOffset + "px";

  let deletePopupMessageId = setTimeout(function () {
    popupMessage.classList.remove("found-elements--visible");
  }, 8000);
}
