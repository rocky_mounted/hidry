export function createProductItem(arr) {
  let productFragment = document.createDocumentFragment();
  let templateProductCard = document.getElementById("template__product-list__item");
  arr.forEach((el) => {
    const itemCard = templateProductCard.content.cloneNode(true);
    itemCard.querySelector("img").src = `img/${el.photo}`;
    itemCard.querySelector("img").alt = `Фото ${el.name}`;
    itemCard.querySelector("h2").textContent = el.name;
    itemCard.querySelector(".product-item__value").textContent = `${el.price} ₽`;
    itemCard.querySelector(".product-item__wrapper").dataset.id = el.id;
    itemCard.querySelector(".product-item__wrapper").href = `components/product_cart.html?id=${el.id}`;
    productFragment.appendChild(itemCard);
  });
  let list = document.querySelector(".product-list");
  list.appendChild(productFragment);
}

export function createSelectedElements(arr) {
  let productFragment = document.createDocumentFragment();
  let templateProductCard = document.getElementById("shopping-list__item");
  let sum = [];
  console.log("отмеченные", arr);
  arr.forEach((el) => {
    const itemElement = templateProductCard.content.cloneNode(true);
    itemElement.querySelector("img").src = `img/${el.photo}`;
    itemElement.querySelector(".shopping-list__title").textContent = el.name;
    itemElement.querySelector( ".shopping-list__item__price").textContent = `${el.price} ₽`;
    itemElement.querySelector(".shopping-list__checkbox").id = `product_${el.id}`;
    itemElement.querySelector("label").setAttribute("for", `product_${el.id}`);
    productFragment.appendChild(itemElement);

    let countProducts = document.querySelector(".countProducts");
    countProducts.textContent = arr.length;
    let priceAllProducts = document.querySelector(".priceAllProducts");

    sum.push(el.price)

    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    priceAllProducts.textContent = `${sum.reduce(reducer, 0)} ₽`;

  });
  let list = document.querySelector(".shopping-list");
  list.appendChild(productFragment);
}







