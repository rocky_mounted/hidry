import { setCount } from "./shoppingCart.js";

function httpGet(url) {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function () {
      if (this.status == 200) {
        resolve(this.response);
      } else {
        var error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function () {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });
}
let featureList = [];
let diseasesList = [];
httpGet("../api/catalog.json").then(
  (response) => {
    const responseData = JSON.parse(response);

    featureList = responseData.featureDictionary;
    diseasesList = responseData.diseasesDictionary;
    // получаем id нужной собаки
    let productId = window.location.search.slice(4);
    let numb = Number(productId);

    // рендерим нужную собаку
    createProductCart(responseData.results[numb]);
    setCount(responseData.results);
  },
  (error) => alert(`Rejected: ${error}`)
);


export function createProductCart(el) {
  let featureList = getFeature(el);
  let diseasesList = getDiseases(el);
  let productFragment = document.createDocumentFragment();
  let templateProductCart = document.querySelector("#mainProduct");
  console.log(templateProductCart)
  const productCart = templateProductCart.content.cloneNode(true);
  productCart.querySelector(".dog-name").textContent = el.name;
  productCart.querySelector("h1").textContent = el.name;
  productCart.querySelector("img").src = `../img/${el.photo}`;
  productCart.querySelector(
    ".product-info__title"
  ).textContent = `Характеристикa ${el.name}`;
  productCart.querySelector(".type").textContent = el.type;
  productCart.querySelector(".size").textContent = `${el.size} кг`;
  productCart.querySelector(".feature").textContent = featureList.join(', ');
  productCart.querySelector(".diseases").textContent = diseasesList.join(", ");
  productCart.querySelector(".intelligence").textContent = el.intelligence;
  productCart.querySelector(
    ".product__trade-price"
  ).textContent = `${el.price} ₽`;
  productFragment.appendChild(productCart);
  let body = document.querySelector("body");
  body.appendChild(productFragment);
}

// работа со словарём

function getFeature(el) {
  let featureArr = []
    el.feature.forEach((item) => {
      const feature = featureList.find((feature) => feature.id == item);
      if (!feature) return;
      featureArr.push(feature.label);
    });
  return featureArr;
}

function getDiseases(el) {
  let diseasesArr = [];
    el.diseases.forEach((item) => {
      const diseases = diseasesList.find((diseases) => diseases.id == item);
      if (!diseases) return;
      diseasesArr.push(diseases.label);
    });
  return diseasesArr;
}

