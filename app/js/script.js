import { createProductItem } from "./renderProductList.js";
import { showPopupMessage } from "./showPopup.js";
import { setCount } from "./shoppingCart.js";
import { createBoxShadow } from "./hw_2.js";

function httpGet(url) {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function () {
      if (this.status == 200) {
        resolve(this.response);
      } else {
        var error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function () {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });
}
let globalData = []
httpGet("../api/catalog.json").then(
  (response) => {
    const responseData = JSON.parse(response);
    globalData = responseData.results;
    createProductItem(responseData.results);

    setCount(responseData.results);

    getFilteredElements(responseData.results);

  },
  (error) => alert(`Rejected: ${error}`)
);

const filterSection = document.querySelector(".filter");
const arrCheckboxInput = filterSection.querySelectorAll("input");

arrCheckboxInput.forEach((item) =>
  item.addEventListener("change", getFilteredElements)
);

function getFilteredElements(arr) {
  console.log(arr)
  // получаем массив типов
  let arrType = [];
  arrCheckboxInput.forEach((item) => {
    if (item.checked && item.name === "typeAnimal") {
      arrType.push(item.value);
    }
  });

  // получаем массив фич
  let arrFeature = [];
  arrCheckboxInput.forEach((item) => {
    if (item.checked && item.name === "featureAnimal") {
      arrFeature.push(item.value);
    }
  });

  // фильтруем по типам
  let arrFiltered = globalData;
  if (arrType.length !== 0) {
    arrFiltered = arrFiltered.filter((item) => {
      return arrType.includes(item.type);
    });
  }

  // фильтруем по фичам
  if (arrFeature.length !== 0) {
    arrFiltered = arrFiltered.filter((item) => {
      return arrFeature.some((it) => item.feature.includes(Number(it)));
    });
  }

  // фильтруем по размерам
  let minSize = document.querySelector("#minSizeValue");
  let maxSize = document.querySelector("#maxSizeValue");

  arrFiltered = arrFiltered.filter((el) => {
    return el.size >= minSize.value && el.size <= maxSize.value;
  });

  getNumberElements(arrFiltered); // показываем колличество найденных элементов
  return arrFiltered;
}

//  показываю попап

arrCheckboxInput.forEach((el) =>
  el.addEventListener("change", showPopupMessage)
);

// рендерю список отфильтрованных пёсиков

const buttonShowList = document.querySelector(".found-elements > button");

function renderNewProductsList(arr) {
  let list = document.querySelector(".product-list");
  let newList = getFilteredElements(arr);
  list.innerHTML = "";
  createProductItem(newList);
  return newList;
}

buttonShowList.addEventListener('click', renderNewProductsList)

// функция для отображения числа отфильтрованных элементов
function getNumberElements(arr) {
  let num = document.querySelector('.found-elements__count')
  num.innerText = ''
  return num.innerText = arr.length
}

document.addEventListener("DOMContentLoaded", function (e) {
  let itemCart = document.querySelectorAll(".product-list__item");;
});



// nj-mood
let buttonNJ = document.querySelector(".nj-mood__button");
buttonNJ.addEventListener("click", createBoxShadow);