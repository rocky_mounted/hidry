export function createBoxShadow() {
  function getElement() {
    let arrElements = document.querySelectorAll("*");
    let item = arrElements[Math.floor(Math.random() * arrElements.length)];
    return item;
  }

  function getRandomColor() {
    let color = Math.floor(Math.random() * 256);
    return color;
  }
  function createStyle() {
    let red = getRandomColor();
    let green = getRandomColor();
    let blue = getRandomColor();
    var boxShadowStyle = `0px 0px 5px 10px rgb(${red}, ${green}, ${blue})`;
    return boxShadowStyle;
  }

  function setShadow() {
    let item = getElement();
    item.style.boxShadow += createStyle();
  }

  function removeShadow() {
    let item = getElement();
    item.style.removeProperty("box-shadow");
  }

  let idIntervalSet = setInterval(setShadow, 2000);
  let idIntervalRemove = setInterval(removeShadow, 2000);

  let removeBtn = document.querySelector(".delete-nj-mood__button");
  removeBtn.classList.add('visible')
  removeBtn.addEventListener('click', function () {
    clearInterval(idIntervalSet);
    clearInterval(idIntervalRemove);
    let arrElements = document.querySelectorAll("*");
    arrElements.forEach(el => el.style.removeProperty('box-shadow'));
    removeBtn.classList.remove("visible");
  })
}




